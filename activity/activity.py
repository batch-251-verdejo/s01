# Create 5 variables and output them in the command prompt in the folowing format
# "I am <name (string)>, and I am <age (integer)>years old, I work as a <occupation (string)>, and my rating for <movie (string)> is <rating (decimal)%"

name, age, occupation, movie, rating, totalrating = "Rowell", 33, "account specialist", "Django unchained", 4.5, 5


print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}/{totalrating}.")

# Create 3 variables, num1, num2, num3

num1, num2, num3 = 5, 10, 15

# a. get the product of num1 and num2
print(num1 * num2)

# b. check if num1 is less than num3
print(num1 < num3)

# c. add the value of num3 to num2
print(num3 + num2)

# Test code
course = 'Intro to Python'
print('Python' in course)

age = 20
print(age)

name = input("What is your name? ")
print("Hello " + name)

birth_year = input("Enter your birth year: ")

age = 2023 - int(birth_year)

print(age)


first = int(input("First: "))
second = int(input("Second: "))
modulo = first % second
print("Modulo: " + str(modulo))